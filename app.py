import pandas as pd
import plotly.express as px  # (version 4.7.0)
import plotly.graph_objects as go

import dash  # (version 1.12.0) pip install dash
import dash_daq as daq
import pandas as pd
import plotly.express as px  # (version 4.7.0)
import plotly.graph_objects as go

import dash  # (version 1.12.0) pip install dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

app = dash.Dash(__name__)

# ---------- Import and clean data (importing csv into pandas)
exercise=pd.read_csv("comsamsungshealthexercise202103241436-210406-164618.csv",header=1)
# deleting the columns that contains only null values
exercise=exercise.dropna(axis=1,how='all')
# changing columns names using Pandas function df.rename()
#exercise=exercise.rename(columns={'com.samsung.health.exercise.deviceuuid':'deviceuuid',
 #                                      'com.samsung.health.exercise.calorie':'calorie'},inplace=False)
#changing names of device ids for convinience
names={
    'F/D7+hL5E5':'Roben',
    'FN3Qt5ROM0':'Patrik',
   'jQfnryI8/B':'Karen'
}
for key, value in names.items():
    exercise.loc[exercise['com.samsung.health.exercise.deviceuuid']==key,'com.samsung.health.exercise.deviceuuid']=value
#changing datatype of time
exercise['com.samsung.health.exercise.start_time']=pd.to_datetime(exercise['com.samsung.health.exercise.start_time'])
exercise['com.samsung.health.exercise.end_time']=pd.to_datetime(exercise['com.samsung.health.exercise.end_time'])
#Using UTC offset, finding the localtime
offset=exercise['com.samsung.health.exercise.time_offset'].str[3:6].astype('int32')
offset_=pd.to_timedelta(offset,'h')
exercise['com.samsung.health.exercise.start_time']=exercise['com.samsung.health.exercise.start_time']+offset_
exercise['com.samsung.health.exercise.end_time']=exercise['com.samsung.health.exercise.end_time']+offset_
# Replacing exercise type numbers by their name

exercise_types={
    0:'Custom type',
    1001:'Walking',
    1002:'Running',
    9002:'Yoga',
    11007:'Cycling',
    13001:'Hiking',
    14001:'Swimming',
    15003:'Exercise bike',
    15004:'Rowing machine',
    15006:'Elliptical trainer',
    16008:'Alpine skiing'
}
for key, value in exercise_types.items():
    exercise.loc[exercise['com.samsung.health.exercise.exercise_type']==key,'com.samsung.health.exercise.exercise_type']=value
exercise['duration_in_minutes']=exercise['com.samsung.health.exercise.duration']/60000
x_df=exercise.groupby([exercise['com.samsung.health.exercise.start_time'].dt.year,'com.samsung.health.exercise.deviceuuid'])[['duration_in_minutes']].mean()
x_df.reset_index(inplace=True)
x_df.head()
# ------------------------------------------------------------------------------
# # App layout
app.layout = html.Div([

    html.H1("Average duration of exercise by year of three ids", style={'text-align': 'center'}),

    dcc.Dropdown(id="slct_id",
                 options=[
                     {"label": "Roben", "value": "Roben"},
                     {"label": "Karen", "value": "Karen"},
                     {"label": "Patrik", "value": "Patrik"}
                     ],
                 multi=False,
                 value="Roben",
                 style={'width': "40%"}
                 ),

    
    html.Br(),

    dcc.Graph(id='exercise_map',figure={}),

])


# # ------------------------------------------------------------------------------
# # Connect the Plotly graphs with Dash Components
@app.callback(
     Output(component_id='exercise_map', component_property='figure'),
    [Input(component_id='slct_id', component_property='value')]
)
def update_graph(option_slctd):
    print(option_slctd)
    print(type(option_slctd))

    
    # Plotly Express
    dff = x_df.copy()
    dff = dff[dff["com.samsung.health.exercise.deviceuuid"] == option_slctd]
    print(dff.head())
    fig = px.bar(
        data_frame=dff,
        x='com.samsung.health.exercise.start_time',
        y='duration_in_minutes',
        hover_data=['com.samsung.health.exercise.start_time', 'duration_in_minutes'],
        labels={'duration_in_minutes': 'Average duration of exercise'},
        template='plotly_dark')
    return fig    

  
# ------------------------------------------------------------------------------
if __name__ == '__main__':
   app.run_server(debug=True)